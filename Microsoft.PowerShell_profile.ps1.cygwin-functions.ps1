# 
# .SYNOPSIS
# 	Finds the first instance of a program in the PATH environment variable.
# 
# .DESCRIPTION
# 	This function uses the cygwin "which" utility to find a program
# 	in the PATH environment variable. The first instance of the program
# 	is reported.
# 
# .PARAMETER program
# 	The program to find on the path.
# 
# .EXAMPLE
# 	which cmd
# 	>> C:\windows\system32\cmd
# 	echo $LastExitCode
# 	>> 0
# 
# .EXAMPLE
# 	which nosuchprogram
# 	>> No nosuchprogram in C:\windows\system32;C:\windows;C:\windows\System32\Wbem;...
# 	echo $LastExitCode
# 	>> 1
# 
function which() {
	param(
		[Parameter(Mandatory=$True)]
		[string] $program
	)
	
	$ErrorActionPreference = "SilentlyContinue"
	$WarningActionPreference = $ErrorActionPreference
	
	$out=$(c:\cygwin\bin\which $program *>&1)
	if ($LastExitCode -eq 0) {
		$out=$(c:\cygwin\bin\cygpath --windows $out)
	} else {
		$out="No $program in $env:PATH"
	}
	
	$out
}

function tail-f() {
	param(
		[Parameter(Mandatory=$True)]
		[string] $filename
	)
	
	$fileStream = New-Object IO.FileStream($filename, [System.IO.FileMode]::Open, [System.IO.FileAccess]::Read, [IO.FileShare]::ReadWrite)
	$reader = new-object System.IO.StreamReader($fileStream)
	$lastMaxOffset = $reader.BaseStream.Length

	while ($true) {
		Start-Sleep -m 100

		#if the file size has not changed, idle
		if ($reader.BaseStream.Length -eq $lastMaxOffset) {
			continue;
		}

		#seek to the last max offset
		$reader.BaseStream.Seek($lastMaxOffset, [System.IO.SeekOrigin]::Begin) | out-null

		#read out of the file until the EOF
		$line = ""
		while (($line = $reader.ReadLine()) -ne $null) {
			write-output $line
		}

		#update the last max offset
		$lastMaxOffset = $reader.BaseStream.Position
	}
}
