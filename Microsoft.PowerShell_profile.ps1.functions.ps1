function prompt {
	"`r`n$env:USERNAME @ $env:COMPUTERNAME ($(Get-Date -Format "yyyy-MM-dd hh:mm:ss zzz")) [$(resolve-path .)]`r`n> "
}

# .Synopsis
# 	Path manipulation function.
# 
# .Parameter cmd
# 	If not specified then this function prints the path, one pathspec per line.
# 	
# 	If specified, must be one of "add", "append", "rm", "remove", "drop", or "insert".
# 	All of these first remove any occurance of the pathspecs from the PATH.
# 	"add" and "append" add each pathspec to the end.
# 	"rm", "remove", and "drop" do nothing else.
# 	"insert" inserts each pathspec to the beginning preserving the order specified
# 	on the command line.
# 
# .Parameter path
# 	Used for debugging. Uses a test PATH value and does not set $env:PATH.
# 	This implies '-print'.
# .Parameter print
# 	Prints the path at the end.
function mod-path () {
	param(
		[Parameter(Mandatory=$False)]
		[string] $path = "",
		
		[Parameter(Mandatory=$False)]
		[switch] $print,
		
		[Parameter(Position=0, Mandatory=$False)]
		[ValidateSet("add","append","rm","remove","drop","insert")]
		[string] $cmd,
		
		[Parameter(Position=1, Mandatory=$False, ValueFromRemainingArguments=$True)]
		[string[]] $pathspecs
	)
	
	$dryRun = $(!!$path)
	if ($dryRun) {
		$PATH = $path
		$print = $True
	} else {
		$PATH = $env:PATH
	}
	
	$pathList = New-Object System.Collections.Generic.List[string]
	"$PATH".split(";") | % {
		$pathList.Add($($_ -replace ('\\*$', '')))
	}
	
	if (!$cmd) {
		$pathList | % { "$_" }
	} else {
		if ($cmd -eq "insert") {
			[array]::Reverse($pathspecs)
		}
		
		write-verbose "path list is $pathList"
		
		$pathspecs | % {
			$pathspec = $_ -replace ('\\*$', '')
			$pathList.RemoveAll(
				{
					$args[0] -eq $pathspec
				}
			) | out-null
			
			write-verbose "$cmd $pathspec over $pathList"
			
			switch -Regex ($cmd) {
				"^(add|append)$" {
					$pathList.Add($pathspec)
					break
				}
				
				"^(rm|remove|drop)$" {
					# Already done
					break
				}
				
				"^insert$" {
					$pathList.Insert(0, $pathspec)
					break
				}
				
				default {
					throw write-error "ruh roh; cmd case not found"
				}
			}
		}
		
		write-verbose "path list is $pathList"
		
		$PATH = [System.String]::Join(";", $pathList)
		if ($print) {
			$PATH
		}
		if (!$dryRun) {
			$env:PATH = $PATH
		}
	}
}

function set-java_home ($java_home) {
	$abs_java_home = [IO.Path]::GetFullPath($java_home)
	if (-not (Test-Path $abs_java_home -pathType container)) {
		throw write-host "Not a Directory: $abs_java_home"
	}
	$abs_java_bin = (join-path $abs_java_home "bin")
	if (-not (test-path (join-path $abs_java_bin "java.exe") -pathType leaf)) {
		throw write-host "No JAVA.EXE in .../bin"
	}
	$env:JAVA_HOME = "$abs_java_home"
	$env:PATH = "$abs_java_bin;$env:PATH"
	write-host "JAVA_HOME = $abs_java_home"
	java.exe -version
}

# http://stackoverflow.com/questions/1183183/path-of-currently-executing-powershell-script
function Get-ScriptDirectory {
    Split-Path $script:MyInvocation.MyCommand.Path
}

filter Format-Xml {
	param(
		[Parameter(ValueFromPipeline = $True)]
		$text
	)
	$sw=New-Object system.io.stringwriter 
	$writer=New-Object system.xml.xmltextwriter($sw) 
	$writer.Formatting = [System.xml.formatting]::Indented 
	$([xml]$text).WriteContentTo($writer) 
	$sw.ToString() 
}


filter Using-Object {
	param (
		[Parameter(ValueFromPipeline = $True, Position = 0)]
		[System.IDisposable]
		$inputObject = $(throw "The parameter -inputObject is required."),
		
		[Parameter(Position = 1)]
		[ScriptBlock]
		$scriptBlock = $(throw "The parameter -scriptBlock is required.")
	)
	
	Try {
		&$scriptBlock
	} Finally {
		if ($inputObject -ne $null) {
			if ($inputObject.psbase -eq $null) {
				$inputObject.Dispose()
			} else {
				$inputObject.psbase.Dispose()
			}
		}
	}
}

function Get-ZipArchive($ZipFile, $Mode = "Read") {
	[System.IO.Compression.ZipFile]::Open($(Get-Item $ZipFile), $Mode)
}

function List-ZipFileContents ($ZipFile) {
	Using-Object ($Archive = $(Get-ZipArchive $ZipFile)) {
		$Archive.Entries | foreach { $_.FullName }
	}
}


function Get-ZipFileContents($ZipFile, $Target) {
	Using-Object ($Archive = $(Get-ZipArchive $ZipFile)) {
		$Archive.Entries | where { $_.FullName -eq $Target } | foreach {
			Using-Object ($stream = $_.Open()) {
				Using-Object ($sw = New-Object system.io.stringwriter) {
					Get-Member -inputObject $stream
				}
			}
		}
	}
}

function get-routes-ipv4() {
	$rtable = Get-WmiObject -class "Win32_IP4RouteTable" -namespace "root\CIMV2"
	
	$rtable | Format-Table -property Nname, Mask, NextHop, Metric1, InterfaceIndex
	
	$nics = Get-WmiObject -Query "SELECT * FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = TRUE"
}

function Download-File($url, $targetFile)
{ 
	Write-Verbose "Downloading $url" 
	$uri = New-Object "System.Uri" "$url" 
	$request = [System.Net.HttpWebRequest]::Create($uri) 
	$request.set_Timeout(15000) #15 second timeout 
	$response = $request.GetResponse() 
	$totalLength = [System.Math]::Floor($response.get_ContentLength()/1024) 
	$responseStream = $response.GetResponseStream() 
	$targetStream = New-Object -TypeName System.IO.FileStream -ArgumentList $targetFile, Create 
	$buffer = new-object byte[] 10KB 
	$count = $responseStream.Read($buffer,0,$buffer.length) 
	$downloadedBytes = $count 
	while ($count -gt 0) { 
		[System.Console]::CursorLeft = 0 
		[System.Console]::Write("Downloaded {0}K of {1}K", [System.Math]::Floor($downloadedBytes/1024), $totalLength) 
		$targetStream.Write($buffer, 0, $count) 
		$count = $responseStream.Read($buffer,0,$buffer.length) 
		$downloadedBytes = $downloadedBytes + $count 
	} 
	Write-Verbose "Finished Download" 
	$targetStream.Flush()
	$targetStream.Close() 
	$targetStream.Dispose() 
	$responseStream.Dispose() 
}

# http://stackoverflow.com/questions/795751/can-i-get-detailed-exception-stacktrace-in-powershell
function Resolve-Error ($ErrorRecord=$Error[0]) {
	$ErrorRecord | Format-List * -Force
	$ErrorRecord.InvocationInfo |Format-List *
	$Exception = $ErrorRecord.Exception
	for ($i = 0; $Exception; $i++, ($Exception = $Exception.InnerException)) {
		"$i" * 80
		$Exception |Format-List * -Force
	}
}

function Write-Callstack([System.Management.Automation.ErrorRecord]$ErrorRecord=$Error[0], [int]$Skip=1) {
	Write-Host # blank line
	if ($ErrorRecord) {
		Write-Host -ForegroundColor Red "$ErrorRecord $($ErrorRecord.InvocationInfo.PositionMessage)"

		if ($ErrorRecord.Exception) {
			Write-Host -ForegroundColor Red $ErrorRecord.Exception
		}

		if ((Get-Member -InputObject $ErrorRecord -Name ScriptStackTrace) -ne $null) {
			#PS 3.0 has a stack trace on the ErrorRecord; if we have it, use it & skip the manual stack trace below
			Write-Host -ForegroundColor Red $ErrorRecord.ScriptStackTrace
			#return
		}
	}

	Get-PSCallStack | Select -Skip $Skip | % {
		Write-Host -ForegroundColor Yellow -NoNewLine "! "
		Write-Host -ForegroundColor Red $_.Command $_.Location $(if ($_.Arguments.Length -le 80) { $_.Arguments })
	}
}

. "$profile.cygwin-functions.ps1"
